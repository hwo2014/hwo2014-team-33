(ns hwo2014bot.ai
	(:use [hwo2014bot.race]
		  [hwo2014bot.curve]
		  [hwo2014bot.track]
		  [hwo2014bot.car]
		  [hwo2014bot.estimate]
		  [hwo2014bot.search]))

(def race-ref (ref {}))

(defn- calibrate
	""
	[throttle stop-fn curve-symbol fit-fn]
	(let [history (atom [])]
		(fn [race]
			(let [my-car (my-car race)
				  velocity (:velocity my-car)
				  dist (:distance my-car)]
				(swap! history conj [(count @history) velocity])
				(if (stop-fn my-car)
					(dosync (println (alter race-ref assoc-in [:constants curve-symbol] (fit-fn @history)) nil))
					throttle)))))


(defn- brake-tick
	""
	[v]
	(let [a (get-in @race-ref [:constants :deacceleration 0])
		  b (get-in @race-ref [:constants :deacceleration 1])]
	(/ (Math/log (/ v a)) (Math/log b))))

(defn- brake-place
	""
	[t]
	(let [a (get-in @race-ref [:constants :deacceleration 0])
		  b (get-in @race-ref [:constants :deacceleration 1])]
		(/ (* a (Math/pow b t)) (Math/log b))))

(defn- brake-dist
	""
	[v0 v1]
	(- (brake-place (brake-tick v1)) (brake-place (brake-tick v0))))

(defn- tick-velocity
	""
	[t]
	(let [a (get-in @race-ref [:constants :acceleration 0])
		  b (get-in @race-ref [:constants :acceleration 1])]
		  (* a (Math/pow b t))))

(defn acc-velocity
	[v t]
	(+ (brake-tick v) t))

(defn- predict-car-pos
	[track car t]
		(let [piece (:piece car)
			  lane (:lane car)
			  piece-distance (:piece-distance car)
			  velocity (:velocity car)
			  lane-length (:length (get-lane track piece lane))
			  next-piece-distance (+ piece-distance (* velocity t))]
			  	(if (> next-piece-distance lane-length)
			  		{:piece (mod (+ piece 1) (count track)) :piece-distance (- next-piece-distance lane-length)}
			  		{:piece piece :piece-distance next-piece-distance})))

(defn- predict-car-velocity
	[track car t]
	{:velocity (+ (:velocity car) (* (:acceleration car) t))})

(defn- predict-car
	[track car t]
	(merge car (predict-car-pos track car t) (predict-car-velocity track car t)))

(defn- drive-to-bend
	""
	[throttle]
	(fn [race]
		(if (= (:type (get-car-lane (:track race) (my-car race))) :bend)
			nil
			throttle)))

(defn- safe-drive
	""
	[]
	(fn [race]
		(println (brake-dist (:velocity (my-car race)) 0.1))
		0.5))

(defn print-race
	""
	[]
	(println @race-ref))

(defn save-track
	""
	[file]
	(try
		(spit (str file) (with-out-str (pr (:track @race-ref))))
		(catch Exception e (println "Failed to save track: " e))))

(defn load-track
	""
	[file]
	(try
		(do
			(read-string (slurp file))
			(dosync (alter race-ref assoc :track (read-string (slurp file)))))
		(catch Exception e (println "Failed to load track " e))))

(defn bend-control
	""
	[race]
	(let  [p (get-in @race-ref [:constants :bend :p])
		   i (get-in @race-ref [:constants :bend :i])
		   d (get-in @race-ref [:constants :bend :d])
		   max-angle (get-in @race-ref [:constants :bend :max-angle])
		   car (my-car race)
		   angle (Math/abs (:angle car))
		   delta (get-delta-zero-angle car)]
		(* 0.5 (/ (:speed (get-car-lane (:track race) car)) (:velocity car)))))
		;(- 1.0 (+ (* p (/ angle max-angle)) (* d (/ delta max-angle))))))

(defn- get-car-state
	""
	[race car]
	(let [time (:time race)
		  speed (:velocity car)
		  angle (:angle car)
		  delta-angle (:delta-angle car)
		  radius (:radius (get-car-lane (:track race) car))
		  ]
	[speed radius angle time]))

(defn- update-estimate
	""
	[est]
	(let [enter-states (atom {})]
		(fn [race]
			(doall 
				(map
					(fn [car-kv]
						(let [car (second car-kv)
							  id (first car-kv)]
							(if (not= (:piece car) (:prev-piece car))
								(let [old-state (get-in @enter-states [id])
									  new-state (get (swap! enter-states assoc id (get-car-state race car)) id)
									  prev-lane (get-lane (:track race) (:prev-piece car) (:lane car))]
									(if-not (or (>(get new-state 0) 10) (nil? old-state))
										(add-observation!
											est
											old-state
											(update-in new-state [3] #(/ (:length prev-lane) (- % (get-in old-state [3]))))
										)
									)
								)
							)))
				(:cars race))))))

(defn- distance-to-next-switch
	""
	[track p]
	(apply map + (map (fn [lanes] (map :length lanes))
		(while-piece track (inc p) (fn [piece] (not (:switch (first piece))))))))

(defn- drive
	""
	[]
	(let [est (atom (estimate 3))
		  update-est-fn (update-estimate @est)]
		(fn [race]
			(update-est-fn race)
			(let [my-car (my-car race)
				  angle (if (contains? my-car :angle) (:angle my-car) 0)
				  input-state (assoc (get-car-state race my-car) 3 10)
				  rs (map :radius (get-lane-range (:track race) my-car 4))
				  best-state (search-speed @est 2 4 input-state rs)
				  target-speed (get best-state 0)
				  current-speed (:velocity my-car)
				  target-throttle (if (nil? target-speed) 1.0 (if (< current-speed (- target-speed 0.1)) 1.0 (/ target-speed 10.0)))]
				;(println (distance-to-next-switch (:track race) (:piece my-car)))
				(if (> current-speed 0) (println (or (nil? target-speed) (< current-speed (* 0.99 target-speed))) input-state best-state rs))
				(cond
					(nil? target-speed) 1.0
					(and (< current-speed 10) (every? #(= % 0) rs)) 1.0
					;(and (contains? my-car :delta-angle) (> (:delta-angle my-car) 3)) 0.0
					(and (< current-speed (* 0.99 target-speed))) (if (> target-throttle 1.0) 1.0 target-throttle)
					:default 0.0)))))

(defn- filter-blocked-lanes
	""
	[race distances]
	(let [cars (:cars race)
		  my-car (my-car race)]

		(map-indexed (fn [lane dist]
			(if (some (fn [car] (and (= (:piece car) (:piece my-car)) (= (:lane car) lane) (> (:piece-distance car) (:piece-distance my-car)))) cars)
				(+ dist 100)
				dist))
			distances)))

(defn lane-ai
	""
	[]
	(let [race @race-ref
		  my-car (my-car race)
		  piece (get (:track race) (:piece my-car))
		  next-lane (get-next-lane (:track race) my-car)]
		(if (and (contains? my-car :piece) (:switch next-lane) (not= (:piece my-car) (:prev-piece my-car)))
			(let [distances (distance-to-next-switch (:track race) (inc (:piece my-car)))
				  min-dist-index (first (apply min-key second (map-indexed vector (filter-blocked-lanes race distances))))]
				(println distances)
				(cond
					(> (:lane my-car) min-dist-index) :left
					(< (:lane my-car) min-dist-index) :right
					:default nil)))))

(defn turbo-ai
	""
	[]
	(let [race @race-ref
		  my-car (my-car race)
		  piece (get (:track race) (:piece my-car))]
		(and (not= (:piece my-car) (:prev-piece my-car)) (contains? (first piece) :turbo))))


(defn- constant-throttle
	""
	[throttle t]
	(let [ticks (atom t)]
		(fn [race] (if (<= (swap! ticks dec) 0) nil throttle))))

(defn crash
	""
	[crash-data]
	(dosync (alter race-ref update-crash crash-data))
	(println (:cars @race-ref)))

(defn spawn
	""
	[spawn-data]
	(dosync (alter race-ref update-spawn spawn-data))
	(println (:cars @race-ref)))

(defn init-ai
	""
	[race-data]
	(dosync (ref-set race-ref (race race-data)))
	(println "AI Init" @race-ref)
	(let [modes (atom [
			;(constant-throttle 1.0 4)
			;(calibrate 1.0 #(>= (:distance %) 50) :acceleration curve-quad)
			;(constant-throttle 0.0 4)
			;(calibrate 0.0 #(<= (:velocity %) 0.1) :deacceleration curve-exp)
			;(constant-throttle 1.0 4)
			(drive)])]
		(fn [cars-data]
			(dosync (alter race-ref update-race cars-data))
			;(println (:cars @race-ref))
			(loop [m @modes]
				(let [val ((first m) @race-ref)]
					(if (nil? val)
						(recur (swap! modes #(drop 1 %)))
						val))))))