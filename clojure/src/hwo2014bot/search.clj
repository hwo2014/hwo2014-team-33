(ns hwo2014bot.search
	(:use [hwo2014bot.estimate]))

(defn search-speed
	""
	[est branch depth input rs]
	(try
		;(println "depth" depth)
		(if (= depth 0)
			input
			(let [branches (make-estimate est (assoc input 1 (first rs)) branch)]
				;(println "Search next state for " input)
				;(println "Best matches " (vec branches))
				(let [best 
					(update-in 
						(reduce (fn [i1 i2] (if (> (get i1 3) (get i2 3)) i1 i2)) (map (fn [in] (search-speed est branch (dec depth) in (rest rs))) branches))
						[3]
						#(min % (get input 3)))
					]
					;(println "Best option" best)
					best)))
		(catch Exception e nil)))