(ns hwo2014bot.race
	(:use [hwo2014bot.car]
		[hwo2014bot.track]))

(defn update-crash
	""
	[race crash-data]
	(let [cars (:cars race)]
		(update-in race [:cars (:color crash-data) :crash] (constantly true))))

(defn update-spawn
	""
	[race crash-data]
	(let [cars (:cars race)]
		(update-in race [:cars (:color crash-data) :crash] (constantly false))))

(defn update-race
	[race cars-data]
	(let [track (:track race)
		  cars (:cars race)]
		(assoc-in (update-in race [:time] inc) [:cars] 
			(into {} (map (fn [car-data] 
				(let [id (:color (:id car-data))
				 	  pos-data (:piecePosition car-data)
				  	  piece (:pieceIndex pos-data)
				  	  dist (:inPieceDistance pos-data)
				  	  lane (:endLaneIndex (:lane pos-data))
				  	  angle (:angle car-data)
				  	  lap (:lap pos-data)]
					(vector id (update-car track (get cars id) piece dist lane angle lap))))
			cars-data)))))

(defn cars
	""
	[cars-data]
	(into {} (map (fn [car-data] (vector (:color (:id car-data)) (car))) cars-data)))

(defn my-car [race]
	(get-in race [:cars (:my-car race)]))

(defn- car-color
	""
	[n cars-data]
	(get-in (first (filter #(= (get-in % [:id :name]) n) cars-data)) [:id :color]))

(defn race
	""
	[race-data]
	{:time 0 :track (track (:track race-data)) :cars (cars (:cars race-data)) :my-car (car-color "Fooba" (:cars race-data)) :constants {:bend {:max-angle 60 :p 0.1 :i 0.01 :d 10.0}}})