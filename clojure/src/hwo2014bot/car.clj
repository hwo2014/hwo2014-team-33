(ns hwo2014bot.car
	(:require [hwo2014bot.track :as track]))

(defn- calc-velocity
	[track p0 d0 p1 d1 l]
	(track/distance track p0 d0 p1 d1 l))

(defn get-lane-range
	""
	[track car c]
	;(println (range c))
	;(println (:piece car))
	;(println (map #(track/get-lane track (+ (:piece car) %) (:lane car)) (range c)))
	(if (nil? (:piece car)) 
		(take c (repeatedly 0))
		(map #(track/get-lane track (+ (:piece car) %) (:lane car)) (range c))))

(defn get-next-lane
	""
	([track car]
	(if (nil? (:piece car))
		nil
		(track/get-lane track (inc (:piece car)) (:lane car))))
	([track car n]
	(track/get-lane track (+ (:piece car) n) (:lane car))))

(defn get-prev-lane
	""
	[track car]
	(println track car)
	(track/get-lane track (dec (:piece car)) (:lane car)))

(defn get-car-lane
	""
	[track car]
	(track/get-lane track (:piece car) (:lane car)))

(defn get-delta-zero-angle
	""
	[car]
	(let [angle (:angle car)
		  prev-angle (:prev-angle car)]
	(if (< angle 0) (- angle prev-angle) (- prev-angle angle))))

(defn dist-to-next-piece
	""
	[track car]
	(- (:length (get-car-lane track car)) (:piece-distance car)))

(defn update-car
	[track car piece dist lane angle lap]
	(let [p0 (:piece car)
		  d0 (:piece-distance car)
		  p1 piece
		  d1 dist
		  l lane
		  a angle
		  ]
	(if (contains? car :piece)
		(let [velocity (calc-velocity track p0 d0 p1 d1 l)
			  acc (- velocity (:velocity car))
			  dist (+ (:distance car) velocity)]
			(merge car {:piece p1 :prev-piece (:piece car) :piece-distance d1 :lane l :angle a :velocity velocity :prev-velocity (:velocity car) :acceleration acc :distance dist :prev-angle (:angle car) :delta-angle (- a (:angle car)) :prev-delta-angle (:delta-angle car) :angle-acc (- (- a (:angle car)) (:delta-angle car)) :lap lap}))
		{:piece p1 :prev-piece p1 :distance d1 :piece-distance d1 :lane l :angle a :velocity 0.0 :acceleration 0.0 :prev-angle 0.0 :delta-angle 0.0 :prev-delta-angle 0.0 :angle-acc 0.0 :lap 0})))

(defn car
	""
	[]
	{:velocity 0.0 :acceleration 0.0 :piece-distance 0.0 :distance 0.0 :crash false})