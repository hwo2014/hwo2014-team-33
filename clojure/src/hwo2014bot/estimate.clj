(ns hwo2014bot.estimate)

(defn estimate
	""
	[k]
	{:dim k :tree (new net.sf.javaml.core.kdtree.KDTree k)})

(defn add-observation!
	""
	[est input output]
	(println "OBSERVE" (vec (take (:dim est) input)) "->" output)
	(.insert (:tree est) (double-array (vec (take (:dim est) input))) output))

(defn make-estimate
	""
	([est input]
		(.nearest (:tree est) (double-array (vec (take (:dim est) input)))))
	([est input n]
		(.nearest (:tree est) (double-array (vec (take (:dim est) input))) n)))