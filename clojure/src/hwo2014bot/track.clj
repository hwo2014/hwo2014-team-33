(ns hwo2014bot.track)

(defn- piece-type
	""
	[piece-data]
	(cond 
		(contains? piece-data :length) :straight
		(contains? piece-data :radius) :bend))

(defn- straight
	""
	[piece-data lanes-data]
	(vec (map (fn [lane-data]
		{:type :straight :length (:length piece-data) :radius 0 :switch (contains? piece-data :switch)})
	lanes-data)))

(defn- bend
	""
	[piece-data lanes-data]
	(vec (map (fn [lane-data]
		(let [a (:angle piece-data)
			  offset (:distanceFromCenter lane-data)
			  r (+ (:radius piece-data) (if (< a 0) offset (- offset)))
		  	  ]
			{:type :bend :length (/ (* (. Math PI) r (Math/abs a)) 180.0) :angle a :radius (if (> a 0) r (- r)) :switch (contains? piece-data :switch)}))
	lanes-data)))

(defn while-piece
	""
	[track p f]
	(take-while f (drop p (cycle track))))


(defn fill-next-bend
	""
	[track]
	(let [input (concat track (take-while (fn [piece] (= (get-in piece [0 :type]) :straight)) track))]
		(loop [rpieces (reverse input) lengths []]
			(let [lane (get (first rpieces) 0)]
				(if (empty? rpieces) lengths
					(if (= (:type lane) :straight)
						(recur (drop 1 rpieces) (conj lengths (+ (if (empty? lengths) 0.0 (last lengths)) (:length lane))))
						(recur (drop 1 rpieces) (conj lengths 0.0))))))))

(defn straight?
	""
	[piece]
	(= (:type (first piece)) :straight))

(defn- max-index
	""
	[coll]
	(first (apply max-key second (map-indexed vector coll))))

(defn mark-turbo
	""
	[track]
	(let [turbo-index
		(max-index 
			(map-indexed
			(fn [i piece]
				(reduce +
					(map :length
						(map first 
							(while-piece 
								track 
								i 
								straight?)))))
			track))]
		(println "TURBO INDEX" turbo-index)
		(update-in track [turbo-index] (fn [lanes] (vec (map #(assoc % :turbo true) lanes))))))
				

(defn get-lane
	""
	[track p l]
	(if (< p 0)
		(get-lane (+ (count track) p) l)
		(get (get track (mod p (count track))) l)))

(defn distance
	""
	[track p0 d0 p1 d1 l]
	(if (= p0 p1) (- d1 d0) (+ d1 (- (:length (get-lane track p0 l)) d0))))

(defn track
	""
	[track-data]
	(mark-turbo
	(let [lanes-data (:lanes track-data)]
		(vec (map (fn [piece-data]
			(case (piece-type piece-data)
				:straight (straight piece-data lanes-data)
				:bend (bend piece-data lanes-data)))
		(:pieces track-data))))))