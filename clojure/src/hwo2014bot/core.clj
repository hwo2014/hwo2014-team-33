(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]]
        [hwo2014bot.ai])
  (:gen-class))

(def ai-fn (ref nil))

(def turbo-available (atom false))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defmulti handle-msg :msgType)

(defmethod handle-msg "carPositions" [msg]
  (let [throttle (ai-fn (get-in msg [:data]))]
    (if (contains? msg :gameTick)
      {:msgType "throttle" :data throttle :gameTick (:gameTick msg)})))

(defmethod handle-msg "crash" [msg]
  (crash (:data msg))
  nil)

(defmethod handle-msg "spawn" [msg]
  (spawn (:data msg))
  nil)

(defmethod handle-msg "gameInit" [msg]
  (dosync (ref-set ai-fn (init-ai (get-in msg [:data :race]))))
  {:msgType "ping" :data "ping"})

(defmethod handle-msg "lapFinished" [msg]
  (println (float (/ (get-in msg [:data :lapTime :millis]) 1000)))
  (print-race)
  nil)

(defmethod handle-msg "gameEnd" [msg]
  (save-track "temp.track")
  nil)

(defmethod handle-msg "turboAvailable" [msg]
  (println "TURBO" msg)
  (swap! turbo-available (constantly true))
  nil)

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping" :gameTick (:gameTick msg)})


(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println (get-in msg [:data :color]) "crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (case (lane-ai)
      :left (do (println "Move left") (send-message channel {:msgType "switchLane" :data "Left" :gameTick (:gameTick msg)}))
      :right (do (println "Move right") (send-message channel {:msgType "switchLane" :data "Right" :gameTick (:gameTick msg)}))
      nil)
    (if (and @turbo-available (turbo-ai))
      (do
        (println "Turbo")
        (swap! turbo-available (constantly false))
        (send-message channel {:msgType "turbo" :data "LEEEEEEEEROY JENKINS!!!"})))
    (let [resp (handle-msg msg)]
      (if (not= resp nil)
        (send-message channel resp)))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))
