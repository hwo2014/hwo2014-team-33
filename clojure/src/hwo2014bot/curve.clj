(ns hwo2014bot.curve)

(defn curve-quad
	""
	[data]
		(vec (.fit
				(reduce #(doto %1 (.addObservedPoint (first %2) (second %2)))
					(new org.apache.commons.math3.fitting.PolynomialFitter (new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer))
				data)
				(double-array [0.1 0.1 0.1])
		)))

(defn curve-exp
	""
	[data]
	(let [log-data (vec (map #(vector (first %) (Math/log (second %))) data))
		  log-curve (curve-quad log-data)]
		(vector (Math/exp (first log-curve)) (Math/exp (second log-curve)))))

(defn microsphere-interpolator
	""
	[input output iterations]
	(println input)
	(println output)
	(let [interpolator (.interpolate (new org.apache.commons.math3.analysis.interpolation.MicrosphereInterpolator) (into-array (map double-array input)) (double-array output))]
		(fn [values]
			(loop [v values i iterations]
				(let [value (.value interpolator (double-array v))]
					(if (= i 0)
						value
						(recur (assoc-in v [1] value) (dec i))))))))